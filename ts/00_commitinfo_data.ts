/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartdns',
  version: '5.0.4',
  description: 'smart dns methods written in TypeScript'
}
